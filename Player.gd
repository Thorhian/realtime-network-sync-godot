extends KinematicBody

var SmallCube = preload("res://SmallCube.gd")
export(float) var FLUID_DENSITY = 0.8
export(float) var SPEED = 20
export(float) var PUSH_IMPULSE = 10
var velocity = Vector3(0,0,0)
var movement = Vector3()
master var remote_movement = Vector3()
puppet var remote_transform = Transform()
puppet var remote_vel = Vector3()

# Client server reconciliation vars
export var csr = true # Client Server Reconciliation
puppet var ack = 0 # Last movement acknowledged
var old_movement = Vector3()
var time = 0
var gravity = -ProjectSettings.get_setting("physics/3d/default_gravity")
func _ready():
	set_network_master(1) #Server is the master

func _physics_process(delta):
	# Server code
	if is_network_master():
		var buoyancy: Vector3 = get_buoyancy()
		velocity += buoyancy*delta
		if csr:
			move_and_slide(velocity + ($InputManager.movement.normalized() * SPEED))
			rpc_unreliable("update_state",transform, get_floor_velocity(), $InputManager.movement_counter)
		else:
			move_and_slide(velocity + (remote_movement.normalized() * SPEED))
			rset_unreliable("remote_transform",transform)
			
		#Applies an impulse to every body overlapping the area
		var overlapping_bodies = $Area.get_overlapping_bodies()
		for body in overlapping_bodies:
			if body is SmallCube:
				var dir = body.translation - self.translation
				dir = dir.normalized()
				var impulse = dir*PUSH_IMPULSE*delta
				body.push(impulse)
	else:
		# Client code
		time += delta
		if csr:
			move_with_reconciliation(delta)
		else:
			transform = remote_transform
			rset_unreliable("remote_movement", $InputManager.movement)

# Hard code bouyancy
func get_buoyancy() -> Vector3:
	# TODO: Get body dimensions using godot API
	var immersed_area = 3 - self.translation.y + 1
	var immersed_volume = 2*2*immersed_area
	var buoyancy = -gravity * immersed_volume * FLUID_DENSITY * 2
	return Vector3(0,buoyancy,0)

func move_with_reconciliation(delta):
	var old_transform = transform
	transform = remote_transform
	var vel = remote_vel
	var movement_list = $InputManager.movement_list
	if movement_list.size() > 0:
		for i in range(movement_list.size()):
			var mov = movement_list[i]
			vel = move_and_slide(mov[2].normalized()*SPEED*mov[1]/delta)
	
	interpolate(old_transform)

func interpolate(old_transform):
	var scale_factor = 0.1
	var dist = transform.origin.distance_to(old_transform.origin)
	var weight = clamp(pow(2,dist/4)*scale_factor,0.0,1.0)
	transform.origin = old_transform.origin.linear_interpolate(transform.origin,weight)

puppet func update_state(t, velocity, ack):
	self.remote_transform = t
	self.remote_vel = velocity
	self.ack = ack