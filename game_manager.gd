extends Node

#var DemoScene = load("res://rpcsv2.tscn")
var DemoScene = load("res://Test.tscn")
var player_id = -1

# Handles scene switching
remote func start_demo():
	var demo = DemoScene.instance()
	demo.print_tree_pretty()
	get_tree().get_root().add_child(demo)
	get_tree().get_root().get_node("Control").queue_free()
	if get_tree().is_network_server():
		# Gives the authority of the input manager to the player.
		demo.get_node("Player/InputManager").set_network_master(player_id)
		rpc("start_demo")
	else:
		# Gives the authority of the input manager to the player
		demo.get_node("Player/InputManager").set_network_master(get_tree().get_network_unique_id())