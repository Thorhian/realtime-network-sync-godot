extends Camera

export(NodePath) var targetPath
var target
var dist = Vector3()

func _ready():
	target = get_node(targetPath)
	dist = target.translation - translation

# Follows the target without jumping on the y axis
func _physics_process(delta):
	translation.x = target.translation.x - dist.x
	translation.z = target.translation.z - dist.z
